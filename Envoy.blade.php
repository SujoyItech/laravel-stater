@servers(['localhost' => '127.0.0.1'])

@story('deploy')
    update-code
    install-dependencies
    queues-websocket-schedule-start
    migration-seed
@endstory

@story('update-deploy')
    update-dependencies
    queues-websocket-schedule-restart
    migration
@endstory

@task('update-code')
    git pull origin master
@endtask

@task('install-dependencies')
    composer install
@endtask

@task('update-dependencies')
    composer update
@endtask

@task('migration')
    php artisan:migrate
@endtask

@task('migration-seed')
    php artisan:migrate --seed
@endtask

@task('queues-websocket-schedule-start')
    php artisan queue:work
    php artisan websocket:serve
    php artisan schedule:work
@endtask

@task('queues-websocket-schedule-restart')
    php artisan queue:restart
    php artisan websockets:serve
    php artisan schedule:work
@endtask

