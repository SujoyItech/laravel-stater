<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Http\Services\Auth\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{
    private $authService;
    public function __construct(AuthService $service) {
        $this->authService = $service;
    }

    public function redirectToProvider($driver) {
        return Socialite::driver($driver)->redirect();
    }

    public function handleProviderCallback($driver) {
        $user = Socialite::driver($driver)->stateless()->user();
        $response = $this->authService->loginWithSocial($user, $driver);
        if ($response->getStatus() == TRUE) {
            if (check_permission(MODULE_SUPER_ADMIN) || check_permission(MODULE_USER_ADMIN)){
                return redirect()->intended('admin/admin-home');
            }elseif (check_permission(MODULE_USER)){
                if (check_role(USER_SELLER)){
                    return redirect()->intended('product-manager-home');
                }else{
                    return redirect()->intended('/');
                }
            }else{
                Auth::logout();
                return redirect()->intended('/');
            }
        } else {
            Auth::logout();
            return redirect()->route('userLogin')->with(['dismiss' => $response->getMessage()]);
        }
    }
}
