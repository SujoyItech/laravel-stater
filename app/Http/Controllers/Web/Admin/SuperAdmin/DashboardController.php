<?php

namespace App\Http\Controllers\Web\Admin\SuperAdmin;


use App\Http\Controllers\Controller;
use App\Http\Services\DashboardService;
use Illuminate\Http\Request;


class DashboardController extends Controller {

    public function index(){
        return view('admin.super_admin.dashboard.home');
    }
}
