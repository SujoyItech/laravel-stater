<?php


namespace App\Http\Modules\Role;

use App\Http\Boilerplate\CustomResponse;
use App\Models\Role\Role;
use App\Models\Role\RoleRoute;

class RoleService {

    public function getRoleData($id){
        $data['role'] = Role::where('id',$id)->first();
        $data['routes'] = RoleRoute::where('module_id',$data['role']->module_id)->get();
        return $data;
    }


    /**
     * @param array $data
     *
     * @return CustomResponse|mixed|string
     */
    public function create(array $data) {
        try {
            $response = Role::create($data);
            return is_null($response) ?
                jsonResponse(false)->message(__('Role create failed')) :
                jsonResponse(true)->message(__('Role has been created successfully'));
        } catch (\Exception $e) {
            return jsonResponse(false)->message($e->getMessage());
        }
    }

    /**
     * @param int $id
     * @param array $data
     *
     * @return CustomResponse|mixed|string
     */
    public function update(int $id, array $data) {
        try {
            $response = Role::where('id',$id)->update($data);
            return $response ?
                jsonResponse(true)->message(__('Role has been updated successfully')) :
                jsonResponse(false)->default();
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function delete($id){
        try {
            $response = Role::where('id',$id)->delete();
            if ($response){
                return jsonResponse(TRUE)->message('Role deleted successfully.');
            }else{
                return jsonResponse(TRUE)->message('Role delete failed.');
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }
}
