<?php

namespace App\Http\Modules\Messaging;
use App\Models\Message\Messaging;

class MessagingRepository
{
    public function getLastAdminMessages($user_id){
        return Messaging::select('messaging.*','receiver.name as receiver_name',
            'sender.name as sender_name','sender.profile_photo_path as sender_image')
               ->join('users as receiver','messaging.receiver','receiver.id')
               ->join('users as sender','messaging.sender','sender.id')
               ->where('messaging.receiver',$user_id)
               ->orderBy('id','desc')
               ->first();
    }

    public function getAdminMessages($user_id){
        return Messaging::select('messaging.*','receiver.name as receiver_name',
            'sender.name as sender_name','sender.profile_photo_path as sender_image')
            ->join('users as receiver','messaging.receiver','receiver.id')
            ->join('users as sender','messaging.sender','sender.id')
            ->where('messaging.receiver',$user_id)
            ->where('messaging.event_type','admin')
            ->orderBy('id','desc')
            ->get();
    }

    public function getAdminMessageById($message_id){
        return Messaging::select('messaging.*','receiver.name as receiver_name',
            'sender.name as sender_name','sender.profile_photo_path as sender_image')
            ->join('users as receiver','messaging.receiver','receiver.id')
            ->join('users as sender','messaging.sender','sender.id')
            ->where('messaging.id',$message_id)
            ->first();
    }
}
