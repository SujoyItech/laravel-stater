<?php

namespace App\Http\Modules\Messaging;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessagingController extends Controller
{
    private $messaging_service;
    public function __construct(MessagingService $messaging_service){
        $this->messaging_service = $messaging_service;
    }

    public function messaging(){
        $last_messagae = $this->messaging_service->getLastAdminMessage(Auth::id());
        if (isset($last_messagae)) {
            $data['message'] = $last_messagae;
            $data['message_details'] = $this->messaging_service->getMessageDetails($last_messagae->message_id);
        }
        $data['my_id'] = Auth::user()->id;
        return view('modules.messaging.admin.messaging',$data);
    }

    public function getEventMessages(Request $request){
       if ($request->type == 'admin') {
            return $this->messaging_service->getAdminMessages(Auth::id());
        }
    }

    public function showMessageDetails(Request $request){
        $message = $this->messaging_service->getAdminMessageById($request->message_id);
        if (isset($message)){
            $data['message'] = $message;
            $message_details_array = $this->messaging_service->getMessageDetails($request->message_id)->toArray();
            $details = $this->messaging_service->prepareMessageDetailsData($message_details_array, $message->event_id);
            $data['message_details'] = $details['message_details'];
            $data['count'] = $details['count'];
            $data['my_id'] = Auth::user()->id;
            return view('modules.messaging.admin.message_details',$data);
        }
    }

    public function loadAuctionMessagesByScroll(Request $request){
        return $this->messaging_service->getAllMessagesFromStorage($request->auction_id, $request->page);
    }

    public function sendMessage(Request $request){
        return $this->messaging_service->sendMessage($request->all());
    }


}
