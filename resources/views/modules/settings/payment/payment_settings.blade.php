@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'paymentSettings'])
@section('content')
    <div class="col-md-12">
        <div class="page-title-box">
            <h4 class="page-title"><i class="fa fa-cogs"></i> {{__('Payment settings')}}</h4>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card-box">
            <div class="text-center mb-3">
                <div class="radio radio-info form-check-inline">
                    <input type="radio" id="brainTree" value="brain_tree" name="payment_method" @if(isset($settings->payment_method) && $settings->payment_method == 'brain_tree') checked @endif class="set-payment-setting">
                    <label for="brainTree"> {{__('Braintree')}} </label>
                </div>
                <div class="radio radio-info form-check-inline">
                    <input type="radio" id="stripe" value="stripe" name="payment_method" @if(isset($settings->payment_method) && $settings->payment_method == 'stripe') checked @endif class="set-payment-setting">
                    <label for="stripe"> {{__('Stripe')}} </label>
                </div>
                <div class="radio radio-info form-check-inline">
                    <input type="radio" id="paypal" value="paypal" name="payment_method" @if(isset($settings->payment_method) && $settings->payment_method == 'paypal') checked @endif class="set-payment-setting">
                    <label for="paypal"> {{__('Paypal')}} </label>
                </div>
            </div>
            <div class="show_payment_settings brain_tree @if(isset($settings->payment_method) && $settings->payment_method == 'brain_tree') @else d-none @endif">
                @include('modules.settings.payment.brain_tree')
            </div>
            <div class="show_payment_settings stripe  @if(isset($settings->payment_method) && $settings->payment_method == 'stripe') @else d-none @endif">
                @include('modules.settings.payment.stripe')
            </div>
            <div class="show_payment_settings paypal @if(isset($settings->payment_method) && $settings->payment_method == 'paypal') @else d-none @endif">
                @include('modules.settings.payment.paypal')
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $('.set-settings').on('blur',function (){
            if ($(this).val().length !== 0){
                const input_name = $(this).attr('name');
                const this_field = $(this);
                const submit_url = "{{route('adminSettingsSave')}}";
                const option_group = "payment_settings";
                const formData = new FormData();
                formData.append('option_type', 'text');
                formData.append('option_group', option_group);
                formData.append('option_key', input_name);
                formData.append('option_value', $(this).val());
                makeAjaxPostFile(formData,submit_url,null,validationResponse).done(function (response){
                    if (response.success == true){
                        this_field.removeClass('is-valid is-invalid').addClass('is-valid');
                        this_field.next().removeClass('invalid-feedback').addClass('valid-feedback');
                        this_field.siblings('.valid-feedback').text('{{__('Looks good!')}}');
                    }else{
                        this_field.removeClass('is-valid is-invalid').addClass('is-invalid');
                        this_field.next().removeClass('valid-feedback').addClass('invalid-feedback');
                        this_field.siblings('.invalid-feedback').text('{{__('Looks bad!')}}');
                    }
                });
            }

        });

        $('.set-payment-setting').on('change',function (){
            if ($(this).val().length !== 0){
                const input_name = $(this).attr('name');
                const active_class = $(this).val();
                const submit_url = "{{route('adminSettingsSave')}}";
                const option_group = "payment_settings";
                const formData = new FormData();
                formData.append('option_type', 'text');
                formData.append('option_group', option_group);
                formData.append('option_key', input_name);
                formData.append('option_value', $(this).val());
                makeAjaxPostFile(formData,submit_url,null).done(function (response){
                    if (response.success == true){
                        $('.show_payment_settings').addClass('d-none');
                        $('.'+active_class).removeClass('d-none');
                    }
                })
            }
        });

        function validationResponse(response){
            $.each(response, function(key,value) {
                $('[name="'+key+'"]').removeClass('is-valid').addClass('is-invalid');
                $('[name="'+key+'"]').next().removeClass('valid-feedback').addClass('invalid-feedback');
                $('[name="'+key+'"]').siblings('.invalid-feedback').text(value[0]);
            });
        }

    </script>
@endsection
