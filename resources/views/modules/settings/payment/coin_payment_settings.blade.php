@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'socialSettings'])
@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('adminSettingsSave')}}" novalidate name="coin_payment_settings" method="POST" id="coin_payment_settings" class="coin_payment_settings_class" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="coin_payment_public_key">{{__('Coin Payment Public Key')}}</label>
                                <input type="text" name="coin_payment_public_key" value="{{ $settings->coin_payment_public_key ?? ''}}"
                                       placeholder="{{__('Coin Payment Public Key')}}" class="form-control coin_payment_settings" id="coin_payment_public_key">
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="coin_payment_private_key">{{__('Coin Payment Private Key')}}</label>
                                <input type="text" name="coin_payment_private_key" value="{{ $settings->coin_payment_private_key ?? ''}}"
                                       placeholder="{{__('Coin Payment Private Key')}}" class="form-control coin_payment_settings" id="coin_payment_private_key">
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="coin_payment_minimum_confirms">{{__('Minimum Confirms')}}</label>
                                <input type="number" name="coin_payment_minimum_confirms" value="{{ $settings->coin_payment_minimum_confirms ?? ''}}"
                                       placeholder="{{__('Minimum Confirms')}}" class="form-control coin_payment_settings" id="coin_payment_minimum_confirms">
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="coin_payment_expiration_time">{{__('Expiration Time (MIN)')}}</label>
                                <input type="number" name="coin_payment_expiration_time" value="{{ $settings->coin_payment_expiration_time ?? ''}}"
                                       placeholder="{{__('Expiration Time')}}" class="form-control coin_payment_settings" id="coin_payment_expiration_time">
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function (){
            resetValidation('coin_payment_settings');
        });

        $('.coin_payment_settings').on('blur',function (){
            if ($(this).val().length !== 0){
                const input_name = $(this).attr('name');
                const this_field = $(this);
                const submit_url = "{{route('adminSettingsSave')}}";
                const option_group = "coin_payment_settings";
                const formData = new FormData();
                formData.append('option_type', 'text');
                formData.append('option_group', option_group);
                formData.append('option_key', input_name);
                formData.append('option_value', $(this).val());
                makeAjaxPostFile(formData,submit_url,null,validationResponse).done(function (response){
                    if (response.success == true){
                        this_field.removeClass('is-valid is-invalid').addClass('is-valid');
                        this_field.next().removeClass('invalid-feedback').addClass('valid-feedback');
                        this_field.siblings('.valid-feedback').text('{{__('Looks good!')}}');
                    }else{
                        this_field.removeClass('is-valid is-invalid').addClass('is-invalid');
                        this_field.next().removeClass('valid-feedback').addClass('invalid-feedback');
                        this_field.siblings('.invalid-feedback').text('{{__('Looks bad!')}}');
                    }
                });
            }

        });

        function validationResponse(response){
            $.each(response, function(key,value) {
                $('[name="'+key+'"]').removeClass('is-valid').addClass('is-invalid');
                $('[name="'+key+'"]').next().removeClass('valid-feedback').addClass('invalid-feedback');
                $('[name="'+key+'"]').siblings('.invalid-feedback').text(value[0]);
            });
        }

    </script>
@endsection
