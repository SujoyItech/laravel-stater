<div class="card-box">
    <h4 class="header-title">{{__('Payment Option List')}}</h4>
    <p class="sub-header">
        {{__('You cann\'t edit the payment option information. If need, Please add new option and remove the unnecessary one.')}}
    </p>
    <div class="table-responsive">
        <table id="payment_option_list" class="table dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th>{{__('Payment Type')}}</th>
                <th>{{__('Account Info')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
