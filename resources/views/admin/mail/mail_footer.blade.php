<tr>
    <td bgcolor="#ffffff">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="padding: 40px 40px 10px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
                    <p style="margin: 0; font-weight: 700; font-size: 16px">Company Address</p>
                </td>
            </tr>
            <tr>
                <td style="padding: 0px 40px 10px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
                    <p style="margin: 0;">This email was sent to you from Company Email Address</p>
                </td>
            </tr>
            <tr>
                <td style="padding: 0px 40px 10px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
                    <p style="margin: 0;"><a href="javascript:void(0)" style="color: #111111; font-weight: 700;">Preferences</a> | <a href="javascript:void(0)" style="color: #111111; font-weight: 700;"> Browser</a> | <a href="javascript:void(0)" style="color: #111111; font-weight: 700;"> Forward</a> | <a href="javascript:void(0)" style="color: #111111; font-weight: 700;"> Unsubscribe</a></p>
                </td>
            </tr>
            <tr>
                <td style="padding: 0px 40px 40px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
                    <p style="margin: 0;">Copyright &copy; 2010-2019 <b>Company Name </b>, All Rights Reserved.</p>
                </td>
            </tr>

        </table>
    </td>
</tr>
