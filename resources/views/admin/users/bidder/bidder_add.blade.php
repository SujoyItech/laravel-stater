<div class="card">
    <div class="card-body ajax-load">
        <h4 class="card-title">{{__('Bidder Add/Edit')}}</h4>
        <form class="bidder-form" novalidate action="{{route('saveBidder')}}" method="post" id="bidder_save">
            <div class="form-group">
                <label for="name">{{__('Name')}}<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{$user->name ?? ''}}" required>
            </div>
            <div class="form-group">
                <label for="email">{{__('Email')}}<span class="text-danger">*</span></label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$user->email ?? ''}}" required>
            </div>
            <div class="form-group">
                <label for="phone">{{__('Phone')}}<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="{{$user->phone ?? ''}}" required>
            </div>
            <div class="form-group">
                <label for="nid">{{__('NID')}}</label>
                <input type="text" class="form-control" id="nid" name="nid" placeholder="{{__('NID')}}" value="{{$user->nid ?? ''}}">
            </div>
            <div class="form-group">
                <label for="tin">{{__('TIN')}}</label>
                <input type="text" class="form-control" id="tin" name="tin" placeholder="{{__('TIN')}}" value="{{$user->tin ?? ''}}">
            </div>
            <div class="form-group">
                <label for="country">{{__('Country')}}<span class="text-danger">*</span></label>
                <select class="form-control" name="country">
                    <option value="">{{__('Select')}}</option>
                    @foreach(countries() as $country)
                        <option value="{{$country}}">{{$country}} {{is_selected($country,$user->country ?? '' )}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="city">{{__('City')}}</label>
                <input type="text" class="form-control" id="city" name="city" placeholder="{{__('City')}}" value="{{$user->city ?? ''}}">
            </div>
            <div class="form-group">
                <label for="state">{{__('State')}}</label>
                <input type="text" class="form-control" id="state" name="state" placeholder="{{__('State')}}" value="{{$user->state ?? ''}}">
            </div>
            <div class="form-group">
                <label for="post_code">{{__('Post code')}}</label>
                <input type="text" class="form-control" id="post_code" name="post_code" placeholder="{{__('State')}}" value="{{$user->post_code ?? ''}}">
            </div>
            <div class="form-group">
                <label for="post_code">{{__('Address')}}</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="{{__('Address')}}" value="{{$user->address ?? ''}}">
            </div>
            <div class="form-group">
                <label for="address_secondary">{{__('Address Secondary')}}</label>
                <input type="text" class="form-control" id="address_secondary" name="address_secondary" placeholder="{{__('Address Secondary')}}" value="{{$user->address_secondary ?? ''}}">
            </div>
            <div class="form-group">
                <label for="address_secondary">{{__('Btc Balance')}}</label>
                <input type="number" class="form-control" id="btc_balance" name="btc_balance" placeholder="{{__('Btc Balance')}}" value="{{$user->btc_balance ?? ''}}">
            </div>
            <div class="form-group">
                <label>{{__('Status')}} <span class="text-danger">*</span></label>
                <select class="form-control" required name="status">
                    <option value="">{{__('Select')}}</option>
                    <option value="{{INACTIVE}}" {{is_selected(INACTIVE,$user->status ?? '')}}>{{__('Inactive')}}</option>
                    <option value="{{ACTIVE}}" {{is_selected(ACTIVE,$user->status ?? '')}}>{{__('Active')}}</option>
                    <option value="{{USER_BLOCKED}}" {{is_selected(USER_BLOCKED,$user->status ?? '')}}>{{__('Block')}}</option>
                    <option value="{{USER_SUSPENDED}}" {{is_selected(USER_SUSPENDED,$user->status ?? '')}}>{{__('Suspend')}}</option>
                </select>
            </div>
            <input type="hidden" name="id" value="{{$user->id ?? ''}}">
            <input type="hidden" name="module_id" value="{{MODULE_USER}}">
            <input type="hidden" name="role" value="{{USER_BIDDER}}">
            <input type="hidden" name="is_seller" value="{{INACTIVE}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form()"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        resetValidation('bidder-form');
        checkSlugVlaidity('user');
    })
</script>
