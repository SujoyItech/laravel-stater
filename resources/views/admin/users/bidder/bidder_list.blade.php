<div class="card-box">
    <h4 class="header-title">{{__('Bidder List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the bidder list')}}
    </p>

    <div class="table-responsive">
        <table id="bidder_table" class="table table-sm table-striped dt-responsive">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Image')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Contact Info')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Verification')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
