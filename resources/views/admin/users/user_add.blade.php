<div class="card">
    <div class="card-body ajax-load">
        <h4 class="card-title">{{__('Admin Add/Edit')}}</h4>
        <form class="user-form" novalidate action="{{route('saveUser')}}" method="post" id="user_save">
            <input type="hidden" name="module_id" value="{{MODULE_USER_ADMIN}}">
            <input type="hidden" name="role" value="{{USER_ADMIN}}">
            <div class="form-group mb-3">
                <label for="name">{{__('Name')}}<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{$user->name ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>

            <div class="form-group mb-3">
                <label for="email">{{__('Email')}}<span class="text-danger">*</span></label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$user->email ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label>{{__('Status')}} <span class="text-danger">*</span></label>
                <select class="form-control" required name="status">
                    <option value="">{{__('Select')}}</option>
                    <option value="{{INACTIVE}}" {{is_selected(INACTIVE,$user->status ?? '')}}>{{__('Inactive')}}</option>
                    <option value="{{ACTIVE}}" {{is_selected(ACTIVE,$user->status ?? '')}}>{{__('Active')}}</option>
                    <option value="{{USER_BLOCKED}}" {{is_selected(USER_BLOCKED,$user->status ?? '')}}>{{__('Block')}}</option>
                    <option value="{{USER_SUSPENDED}}" {{is_selected(USER_SUSPENDED,$user->status ?? '')}}>{{__('Suspend')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group">
                <label for="address_secondary">{{__('Btc Balance')}}</label>
                <input type="number" class="form-control" id="btc_balance" name="btc_balance" placeholder="{{__('Btc Balance')}}" value="{{$user->btc_balance ?? ''}}">
            </div>
            <input type="hidden" name="id" value="{{$user->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form()"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        resetValidation('user-form');
        checkSlugVlaidity('user');
    })
</script>
